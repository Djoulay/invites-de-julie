'use strict';

// Gestion des invitée.
var count = 0;
// les invités (modèle)
var people = [];// on déclare un tableau vide pour stocker le invités

var total = 0;

var totalMen = 0;

var totalWomen = 0;

var countTotal = document.getElementById('countEuro');

var countElement = document.getElementById('count');

var countMen = document.getElementById('men');

var countWomen = document.getElementById('women');



// lors d'un click sur bouton
var validBtn = document.getElementById('valid-btn');
if (!validBtn) {
  console.error('Bouton validation manquant');
} else {
  validBtn.onclick = function (event) {

    if (count >= 10) {
      document.getElementById('max-count-error').style.display = 'block';// modif du css en block pour voir le message
      event.target.disabled = true;//désactive le bouton quand =true
      return;// stop l'ajout d'invité
    }

    // récupère le nom saisie
    var inputName = document.getElementsByTagName('input')[0];//on récupère la  première valeur de l'input dans le tableau
    if (!inputName) {
      console.error('champs de saisie manquant');
      // on arrête l'execution de la fonction
      return;
    }

    var inputEuro = document.getElementsByTagName('input')[1];//?????????????????????????????????????????????????
    if (!inputEuro) {
      console.error("si tu veux boire de l'eau c'est toi qui vois...");
    }

    var inputMen = document.getElementsByTagName('input')[2];//récup homme

    var inputWomen = document.getElementsByTagName('input')[3];//récup femme

    // mise à jour du modèle
    people.push(inputName.value + " a donné " + inputEuro.value + " euros ");// on ajoute la valeur saisie
    console.log('people: ', people);// affichage dans la console



    // mise à jour de la vue de la liste
    updateListView(people);// rappel de la fonction

    count++;
    countElement.innerText = count;//on affiche le nbr d'invités en haut de la page


    //incrémentation cagnote
    total = parseInt(inputEuro.value) + total;
    countTotal.innerHTML = total;

    //incrémentation homme
    totalMen++;
    countMen.innerText = totalMen;

    //incrémentation femme
    totalWomen++;
    countWomen.innerText = totalWomen;
  };
}





// mise à jour de la vue
function updateListView(people) {

  var liste = document.getElementById('people');

  // on purge la liste
  while (liste.firstChild) {
    liste.removeChild(liste.firstChild);// on sort le premier enfant du parent liste pas compris!!!!!!!!!!!!!!!!!!!
  }

  // et on la reconstruit à partir du modèle (tableau) pas compris!!!!!
  // item par item
  for (var i = 0; i < people.length; i++) {
    var newItem = document.createElement('li');

    // on stock dans l'élément HTML l'index du tableau people correspondant dans le modèle
    newItem.setAttribute('data-index', i);//pas compris!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    // mettre nom saisie dans element liste

    newItem.innerText = people[i];


    //constructeur date

    var d = new Date();//on créé un objet de type date

    var dy = d.getFullYear();//on li la propriété avec GET qui est une méthode
    var dmo = d.getMonth();
    var dday = d.getDay();
    var ddate = d.getDate();
    var dh = d.getHours();
    var dmi = d.getMinutes();
    var ds = d.getSeconds();

    var mois = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'];
    var jour = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
    var dateFinale = ("le " + jour[dday] + " " + ddate + " " + mois[dmo] + " " + dy + " à " + dh + "h " + dmi + "min " + ds + "s ");


    var dateInfo = document.createElement('span');
    dateInfo.className = 'date';
    dateInfo.innerText = dateFinale;

    var removeBtn = document.createElement('button');//creation du bouton suppr
    removeBtn.className = 'remove-item';//affectation de la class
    removeBtn.innerText = 'X';//représenté par une croix

    // handler sur click bouton remove
    removeBtn.onclick = function (event) {

      // MISE A JOUR DU MODEL
      // on récupère l'attribut data-index du parent (li) pour spécifier 
      // l'élément du tableau à supprimer (l'index).
      people.splice(event.target.parentElement.attributes.getNamedItem('data-index').value, 1);
      count--;

      //décrémentation
      var inputEuro = document.getElementsByTagName('input')[1];
      total = total - parseInt(inputEuro.value);

      console.log('people ', people);

      // MISE A JOUR DE LA VUE
      // On va demander au parent du parent du bouton cliqué
      // (le ul donc), de supprmier le parent du bouton (li)
      // event.target.parentElement.parentElement.removeChild(event.target.parentElement);
      updateListView(people);
      countElement.innerText = count;
      countTotal.innerText = total;//ne pas oublier d'afficher ce qu'on a fait!

      // mise à jour bouton valid
      validBtn.disabled = false;
      document.getElementById('max-count-error').style.display = '';
    }

    newItem.appendChild(dateInfo);
    newItem.appendChild(removeBtn);



    // ajouter element dans la liste
    liste.appendChild(newItem);

  }
}





